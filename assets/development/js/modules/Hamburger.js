
class Hamburger {

    constructor() {
        this.hamburgerSelector = $('.hamburger');
        this.navigationSelector = $('.navigation');
    }

    hideMenu() {
        let navigationWidth = +this.navigationSelector.outerWidth(true);

        this.navigationSelector.animate({
            left: navigationWidth - 2 * navigationWidth
        }, 500, () => {
            this.hamburgerSelector.removeClass('opened is-active');
            this.navigationSelector.removeClass('opened');
        });
    }

    openMenu() {
        this.navigationSelector.animate({
            left: 0
        }, 500, () => {
            this.hamburgerSelector.addClass('opened is-active');
            this.navigationSelector.addClass('opened');
        });
    }

    handleEvent() {
        this.hamburgerSelector.on('click', (event) => {
            event.preventDefault();

            let isOpen = this.hamburgerSelector.hasClass('opened') &&
                this.navigationSelector.hasClass('opened');

            if (true === isOpen) {
                this.hideMenu();
            } else {
                this.openMenu();
            }
        });
    }

    init() {
        this.handleEvent();
    }
}

export { Hamburger as default };