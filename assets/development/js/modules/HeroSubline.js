import Typed from 'typed.js';

class HeroSubline {

    /**
     * @returns {{}}
     */
    get settings() {
        return {
            typeSpeed: 40,
            stringsElement: '#hero-strings',
            loop: true,
            backDelay: 3000
        }
    }

    /**
     * @param $selector
     * @returns {Typed}
     */
    init($selector) {
        let randomClassName = 'typed' + Math.floor(Math.random() * 1000001);
        $selector.addClass(randomClassName);

        return new Typed(
            '.' + randomClassName,
            this.settings
        );
    }
}

export { HeroSubline as default };