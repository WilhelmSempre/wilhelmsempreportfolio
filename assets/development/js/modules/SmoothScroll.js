import SmoothScrollPolyfills from 'smooth-scroll/dist/smooth-scroll.polyfills';

class SmoothScroll {

    /**
     * @returns {{}}
     */
    get settings() {
        return {

        }
    }

    init() {
        new SmoothScrollPolyfills('a[href*="#"]', this.settings);
    }
}

export { SmoothScroll as default };