import OwlCarousel from 'owl.carousel';

class Carousel {

    constructor() {
        this.carouselSelector = $('.owl-carousel');

        this.settings = {
            items: 1,
            dotsContainer: '.carousel-dots',
            navContainer: '.carousel-nav',
            autoplay: true,
            loop: true,
            dots: false,
            autoplayHoverPause: true,
            navText: [
                '<i class="fas fa-arrow-left"></i>',
                '<i class="fas fa-arrow-right"></i>'
            ],
            responsive: {
                480: {
                    nav: true,
                    dots: true,
                }
            }
        };
    }

    /**
     * @param $parentElement
     * @param index
     */
    createNav($parentElement, index) {
        return new Promise((resolve) => {
            $parentElement.append('<div id="nav' + index + '" class="owl-nav owl-nav' + index + '"></div>');
            resolve();
        });
    }

    /**
     * @param $parentElement
     * @param index
     */
    createDots($parentElement, index) {
        return new Promise((resolve) => {
            $parentElement.append('<div id="dots' + index + '" class="owl-dots owl-dots' + index + '"></div>');
            resolve();
        })
    }

    init() {
        let that = this;

        this.carouselSelector.each(function (index) {
            let $parentElement = $(this).parent();

            that.createNav($parentElement, index).then(
                that.createDots($parentElement, index)
            ).finally(() =>{
                let settings = [];

                settings[index] = $.extend(that.settings, {
                    dotsContainer: '.owl-dots' + index,
                    navContainer: '.owl-nav' + index,
                });

                console.log(settings[index]);

                $(this).owlCarousel(settings[index]);
            })
        });
    }
}

export { Carousel as default };