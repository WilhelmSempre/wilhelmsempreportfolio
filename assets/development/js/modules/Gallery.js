class Gallery {

    /**
     * @returns {{duration: number}}
     */
    get settings() {
        return {
           duration: 200,
        }
    }

    /**
     * @param $galleryWrapper
     */
    hide($galleryWrapper) {
        $galleryWrapper.fadeOut(this.settings.duration, () => {
            let $closeButton = $galleryWrapper.find('[data-close]');

            $closeButton.off('click');
        });
    }

    /**
     * @param $galleryWrapper
     */
    show($galleryWrapper) {
        $galleryWrapper.fadeIn(this.settings.duration, () => {
            let $closeButton = $galleryWrapper.find('[data-close]');

            $closeButton.on('click', (event) => {
                event.preventDefault();

                this.hide($galleryWrapper);
            });
        });
    }

    /**
     * @param $selector
     */
    handleEvent($selector) {
        let that = this;

        $selector.on('click', (event) => {
            event.preventDefault();

            let $galleryWrapper = $(event.currentTarget).find('[data-gallery]');

            if ($galleryWrapper.length === 0) {
                console.error('Gallery: Gallery wrapper not found! Aborting!');
                return false;
            }

            if (!$galleryWrapper.is(':visible')) {
                that.show($galleryWrapper);
            }
        });
    }

    /**
     * @param $selector
     */
    init($selector) {
        this.handleEvent($selector);
    }
}

export { Gallery as default };