import HeroSubline from './modules/HeroSubline';
import Hamburger from './modules/Hamburger';
import Carousel from './modules/Carousel';
import SmoothScroll from './modules/SmoothScroll';
import Gallery from './modules/Gallery';

class WilhelmSemprePortfolio {

    loadModules() {

        /** Subline text **/
        let HeroSublineObject = new HeroSubline();
        $('.typed').each(function () {
            HeroSublineObject.init($(this));
        });

        /** WOW **/
        const WOW = require('wowjs');
        new WOW.WOW().init();

        /** Hamburger **/
        const HamburgerObject = new Hamburger();
        HamburgerObject.init();

        /** Carousel **/
        const CarouselObject = new Carousel();
        CarouselObject.init();

        /** SmoothScroll **/
        const SmoothScrollObject = new SmoothScroll();
        SmoothScrollObject.init();

        /** Gallery **/
        const GalleryObject = new Gallery();
        GalleryObject.init($('.workspace .work'));
    }

    init() {
        this.loadModules();
    }
}

new WilhelmSemprePortfolio().init();